package streamApi;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class StreamApi {
	public static void main(String[] args) {
		//Stream APIを使う場合
		List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5);
		integerList.stream() // streamの取得
			.filter(i -> i % 2 == 0) // 中間操作
			.forEach(i -> System.out.println(i)); // 終端操作

		//リストの作成
		List<Integer> numbers = List.of(3, 1, -4, 1, -5, 9, -2, 6, 5, 3, 5);
		Stream<Integer> streamList = numbers.stream();

		//配列の作成
		int[] array = {3, 1, -4, 1, -5, 9, -2, 6, 5, 3, 5};
		IntStream streamArrays = Arrays.stream(array);

		//2倍にして表示
		List<Integer> numbersList = List.of(3, 1, -4, 1, -5, 9, -2, 6, 5, 3, 5);
		numbersList.stream()
		.map(number -> number * 2)
		.forEach(System.out::println);
		System.out.println(numbers);

		//0より大きいものを表示
		List<Integer> numbersFilter = List.of(3, 1, -4, 1, -5, 9, -2, 6, 5, 3, 5);
		numbersFilter.stream()
		.filter(number -> number > 0)
		.forEach(System.out::println);
		System.out.println(numbers);

		//降順表示
		List<Integer> numbersSort = List.of(3, 1, -4, 1, -5, 9, -2, 6, 5, 3, 5);
		numbersSort.stream()
		.map(number -> number * 2)
		.sorted((number1, number2) -> number2 - number1)
		.forEach(System.out::println);

		//collectorsを使用して別のリストに結果を入れる
		List<Integer> numbersCollector = List.of(3, 1, -4, 1, -5, 9, -2, 6, 5, 3, 5);
		List<Integer> result = numbersCollector.stream()
		.filter(number -> number > 0)
		.collect(Collectors.toList());
		System.out.println(result);

		//"positive"と"zero or negative"のキーを指定して、Mapとして表示
		List<Integer> numbersMap = List.of(3, 1, -4, 1, -5, 9, -2, 6, 5, 3, 5);
		Map<String, List<Integer>> results = numbersMap.stream()
		.collect(Collectors.groupingBy(number -> number > 0 ? "positive" : "zero or negative"));
		System.out.println(results);
	}

}
