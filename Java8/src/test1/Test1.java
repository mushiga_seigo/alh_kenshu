package test1;

import java.util.Arrays;
import java.util.List;

@FunctionalInterface
interface Color1 {
	String write1(String a);
}

public class Test1 {
	public static void main(String[] args) {

		Color1 c1 = (String a) -> {return a + "です";};

		String b = c1.write1("⻘");
		System.out.println(b); //⻘です


		List<Integer> numbers = Arrays.asList(3, 1, -4, 1, -5, 9, -2, 6, 5, 3, 5);
		numbers.sort((v1, v2) -> Math.abs(v1) - Math.abs(v2));

		List<Integer> numbersFor = List.of(3, 1, -4, 1, -5, 9, -2, 6, 5, 3, 5);
		numbersFor.stream()
		.filter(number -> Math.abs(number) >= 5)
		.forEach(System.out::println);
		System.out.println(numbersFor);

		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
		int total =list.stream()
		.filter(x -> x >= 2)
		.mapToInt(x -> x).sum();
		System.out.println("Total : " + total);
	}
}