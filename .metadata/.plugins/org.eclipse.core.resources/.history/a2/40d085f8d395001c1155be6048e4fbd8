package chapter6.service;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.csv.CsvDataSet;
import org.dbunit.dataset.csv.CsvDataSetWriter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import beans.Branch;
import chapter6.beans.Message;
import chapter6.dao.UserMessageDao;
import junit.framework.TestCase;
import sample.BranchDbUtil;

public class MessageServiceTest extends TestCase {

	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost/simple_twitter_database";
	private static final String USER = "root";
	private static final String PASSWORD = "root";

	public MessageServiceTest(String name) {
		super(name);
	}

	private File file;

	//DB接続部分(DBUtil)
	static {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static Connection getConnection() throws Exception {
		Class.forName(DRIVER);
		Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
		return connection;
	}


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

		//(1)IDatabaseConnectionを取得
		IDatabaseConnection connection = null;
		try {
			super.setUp();
			Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			//(2)現状のバックアップを取得
			QueryDataSet partialDataSet = new QueryDataSet(connection);
			partialDataSet.addTable("message");
			partialDataSet.addTable("message_out");

			//バックアップをcsvで出力
			CsvDataSetWriter.write(partialDataSet,new File("backup"));

			file = File.createTempFile("message", ".xml");
			FlatXmlDataSet.write(partialDataSet,
					new FileOutputStream(file));

			//csv用データ投入
			IDataSet dataSetMessage = new CsvDataSet(new File("dataset"));
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetMessage);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}
		}
	}

	/**
	 * テスト後の片付け
	 */
	@After
	public void tearDown() throws Exception {

		IDatabaseConnection connection = null;
		try {
			super.tearDown();
			Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			IDataSet dataSet = new FlatXmlDataSet(file);
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			//一時ファイルの削除
			if (file != null) {
				file.delete();
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}

		}

	}

	/**
	 * 参照メソッドのテスト
	 */
	@Test
	public void testAllMessage() throws Exception {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		Message message = new Message();

		//参照メソッドの実行
		UserMessageDao().select(connection, LIMIT_NUM)
		Message resultMessage = MessageService.insert(message);

		//値の検証

		//件数
		assertEquals(3, resultMessage.size());

		//データ
		Message result1 = resultMessage.get();
		assertEquals("id=1", "id=" + result1.getId());
		assertEquals("text=こんにちは", "text=" + result1.getText());
	}

	/**
	 * 更新メソッドのテスト
	 */
	@Test
	public void testUpdataMessage() throws Exception {

		//テスト対象となる、storeメソッドを実行
		//テストのインスタンスを生成
		Branch branch001 = new Branch();
		branch001.setBranchCode("001");
		branch001.setBranchName("札幌支店");
		branch001.setBranchSale(13000);

		Branch branch002 = new Branch();
		branch002.setBranchCode("002");
		branch002.setBranchName("北極支店");
		branch002.setBranchSale(7000);

		Branch branch003 = new Branch();
		branch003.setBranchCode("003");
		branch003.setBranchName("南極支店");
		branch003.setBranchSale(13000);

		List<Branch> branchSetList = new ArrayList<Branch>();
		branchSetList.add(branch001);
		branchSetList.add(branch002);
		branchSetList.add(branch003);

		BranchDbUtil.updataBranch(branchSetList);

		//テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
		IDatabaseConnection connection = null;
		try {

			Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			//メソッド実行した実際のテーブル
			IDataSet databaseDataSet = connection.createDataSet();
			ITable actualTable = databaseDataSet.getTable("branch_sale_out");

			// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
			IDataSet expectedDataSet = new FlatXmlDataSet(new File("branch_test_data2.xml"));
			ITable expectedTable = expectedDataSet.getTable("branch_sale_out");

			//期待されるITableと実際のITableの比較
			Assertion.assertEquals(expectedTable, actualTable);
		} finally {
			if (connection != null)
				connection.close();
		}

	}


}
