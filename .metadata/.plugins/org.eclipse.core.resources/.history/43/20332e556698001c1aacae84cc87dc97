package chapter6.service;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.dbunit.Assertion;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.csv.CsvDataSet;
import org.dbunit.dataset.csv.CsvDataSetWriter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import chapter6.beans.User;
import junit.framework.TestCase;

public class UserDbUnitTest extends TestCase  {

	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost/simple_twitter_database";
	private static final String USER = "root";
	private static final String PASSWORD = "root";

	public UserDbUnitTest(String name) {
		super(name);
	}

	private File file;

	//DB接続部分(DBUtil)
	static {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static Connection getConnection() throws Exception {
		Class.forName(DRIVER);
		Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
		return connection;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		//(1)IDatabaseConnectionを取得
		IDatabaseConnection connection = null;
		try {
			super.setUp();
			Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			//(2)現状のバックアップを取得
			QueryDataSet partialDataSet = new QueryDataSet(connection);
			partialDataSet.addTable("users");
			partialDataSet.addTable("user_out");

			//バックアップをcsvで出力
			CsvDataSetWriter.write(partialDataSet,new File("userBackup"));

			file = File.createTempFile("users", ".xml");
			FlatXmlDataSet.write(partialDataSet,
					new FileOutputStream(file));

			//csv用データ投入
			IDataSet dataSetUser = new CsvDataSet(new File("userDataset"));
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetUser);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}
		}
	}

	/**
	 * テスト後の片付け
	 */
	@After
	public void tearDown() throws Exception {

		IDatabaseConnection connection = null;
		try {
			super.tearDown();
			Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			IDataSet dataSet = new FlatXmlDataSet(file);
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			//一時ファイルの削除
			if (file != null) {
				file.delete();
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}

		}

	}

	/**
	 * 参照メソッドのテスト
	 */
	@Test
	public void testUser() throws Exception {

		//参照メソッドの実行
		User resultUser = UserService.select(1);

		//値の検証

		//データ
		User result1 = resultUser;
		assertEquals("id=1", "id=" + result1.getId());
		assertEquals("account=ユーザーアカウント", "account=" + result1.getAccount());
		assertEquals("name=ユーザーネーム", "name=" + result1.getName());
		assertEquals("email=test@mail", "email=" + result1.getEmail());
		assertEquals("password=password", "password=" + result1.getPassword());
		assertEquals("description=こんにちは", "description=" + result1.getDescription());
	}

	/**
	 * 更新メソッドのテスト
	 */
	@Test
	public void testUpdataUser() throws Exception {

		//テスト対象となる、storeメソッドを実行
		//テストのインスタンスを生成
		User user1 = new User();
		user1.setId(1);
		user1.setAccount("ユーザーアカウント");
		user1.setName("ユーザーネーム");
		user1.setEmail("email");
		user1.setPassword("password");
		user1.setDescription("こんにちは");

		UserService.update(user1);

		//テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
		IDatabaseConnection connection = null;
		try {

			Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			//メソッド実行した実際のテーブル
			IDataSet databaseDataSet = connection.createDataSet();
			ITable actualTable = databaseDataSet.getTable("user_out");

			// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
			IDataSet expectedDataSet = new FlatXmlDataSet(new File("user_test_data2.xml"));
			ITable expectedTable = expectedDataSet.getTable("user_out");

			//期待されるITableと実際のITableの比較
			Assertion.assertEquals(expectedTable, actualTable);
		} finally {
			if (connection != null)
				connection.close();
		}

	}

}