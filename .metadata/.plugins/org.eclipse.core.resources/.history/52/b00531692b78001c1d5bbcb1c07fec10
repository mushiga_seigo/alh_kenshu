package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String COMMODITY_FILE_NOT_EXIST = "商品定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String COMMODITY_FILE_INVALID_FORMAT = "商品定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String AMOUNT_EXCEEDED = "合計金額が10桁を超えました";
	private static final String SALE_FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String SALE_FILE_INVALID_CODE = "の支店コードが不正です";
	private static final String SALE_FILE_INVALID_COMMODITY_CODE = "の商品コードが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数が1つ設定されていなければエラー表示
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}+$", FILE_INVALID_FORMAT, FILE_NOT_EXIST)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[0-9a-zA-Z]{8}$", COMMODITY_FILE_INVALID_FORMAT, COMMODITY_FILE_NOT_EXIST)) {
			return;
		}

		//集計処理
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//売上ファイルの判別処理
		for (int i = 0; i < files.length; i++) {
			String filesName = files[i].getName();
			//ファイルなのかディレクトリなのかを判断
			//ファイル名が数字8桁で拡張子がrcdか判断
			if (files[i].isFile() && filesName.matches("^[0-9]{8}\\.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//売上ファイルの並び替え(昇順)
		Collections.sort(rcdFiles);
		//売上ファイルが連番になっているか確認
		for(int i = 0; i < rcdFiles.size() - 1; i++) {

			//File型をString型に変換してから、int型にして数字を抜き取る
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if(latter - former != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		//売上ファイルの読み込み、抽出処理
		BufferedReader br = null;

		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				List<String> rcdValues = new ArrayList<>();
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;
				while((line = br.readLine()) != null) {
					rcdValues.add(line);
				}

				//売上ファイルのフォーマット（3行になっているか）が異なればエラーを表示
				if(rcdValues.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + SALE_FILE_INVALID_FORMAT);
					return;
				}

				//売上ファイルの支店コードがリストになければエラーを表示
				if(!branchSales.containsKey(rcdValues.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + SALE_FILE_INVALID_CODE);
					return;
				}

				//売上ファイルの商品コードがリストになければエラーを表示
				if(!commoditySales.containsKey(rcdValues.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + SALE_FILE_INVALID_COMMODITY_CODE);
					return;
				}

				//売上データが数字で構成されているかを判断
				if(!rcdValues.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//売上加算処理
				//fileSales(売上データ)のString型をLong型に変換
				long fileSales = Long.parseLong(rcdValues.get(2));
				long saleAmount = branchSales.get(rcdValues.get(0)) + fileSales;
				long commoditySaleAmount = commoditySales.get(rcdValues.get(1)) + fileSales;

				//支店別売上集計額が10桁を超えた場合エラーを表示
				if(saleAmount >= 10000000000L) {
					System.out.println(AMOUNT_EXCEEDED);
					return;
				}

				//商品別売上集計額が10桁を超えた場合エラーを表示
				if(commoditySaleAmount >= 10000000000L) {
					System.out.println(AMOUNT_EXCEEDED);
					return;
				}
				//売上データの金額を置き換える処理
				branchSales.replace(rcdValues.get(0), saleAmount);
				commoditySales.replace(rcdValues.get(1), commoditySaleAmount);
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}


	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @param 商品コードと商品名を保持するMap
	 * @param 商品コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales, String regex, String formatError, String existError) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//ファイルがなければエラー文を出力して戻る
			if(!file.exists()) {
				System.out.println(existError);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				//データが[,]で区切られていないか、支店コードが3桁でなければエラー表示し戻る
				//商品定義ファイルの場合[,]で区切られていないか、8桁のローマ字と数字の組み合わせ出なければエラー表示
				if((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(formatError);
					return false;
				}

				//Mapに値を格納
				Names.put(items[0], items[1]);
				Sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @param 商品コードと商品名を保持するMap
	 * @param 商品コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales) {
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			//ファイル作成処理
			file.createNewFile();
			//書き込み処理
			FileWriter fr = new FileWriter(file);
			bw = new BufferedWriter(fr);

			for(String key : Names.keySet()) {
				bw.write(key + "," + Names.get(key) + "," + Sales.get(key).toString());
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
