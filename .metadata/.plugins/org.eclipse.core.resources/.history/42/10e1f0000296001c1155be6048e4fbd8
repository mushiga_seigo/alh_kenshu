package chapter6.service;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import chapter6.beans.UserMessage;
import junit.framework.TestCase;

public class MessageDbUnitTest extends TestCase {

	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost/simple_twitter_database";
	private static final String USER = "root";
	private static final String PASSWORD = "root";

	public MessageDbUnitTest(String name) {
		super(name);
	}

	private File file;

	//DB接続部分(DBUtil)
	static {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static Connection getConnection() throws Exception {
		Class.forName(DRIVER);
		Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
		return connection;
	}

	@Before
	public void setUp() throws Exception {

		//(1)IDatabaseConnectionを取得
		IDatabaseConnection connection = null;
		try {
			super.setUp();
			Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			//(2)現状のバックアップを取得
			QueryDataSet partialDataSet = new QueryDataSet(connection);
			partialDataSet.addTable("message");
			partialDataSet.addTable("message_out");

			file = File.createTempFile("message", ".xml");
			FlatXmlDataSet.write(partialDataSet,
					new FileOutputStream(file));

			//(3)テストデータを投入する
			IDataSet dataSet
			= new FlatXmlDataSet( new File("message_test_data.xml"));
			DatabaseOperation.CLEAN_INSERT.execute(connection,dataSet);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}
		}
	}

	/**
	 * テスト後の片付け
	 */
	@After
	public void tearDown() throws Exception {

		IDatabaseConnection connection = null;
		try {
			super.tearDown();
			Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			IDataSet dataSet = new FlatXmlDataSet(file);
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			//一時ファイルの削除
			if (file != null) {
				file.delete();
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}

		}

	}

	/**
	 * 参照メソッドのテスト
	 */
	@Test
	public void testAllMessage() throws Exception {

		//参照メソッドの実行
		List<UserMessage> resultMessages = MessageService.select();

		//値の検証

		//件数
		assertEquals(3, resultMessages.size());

		//データ
		UserMessage result1 = resultMessages.get(0);
		assertEquals("id=1", "id=" + result1.getId());
		assertEquals("UserId=1", "UserId=" + result1.getUserId());
		assertEquals("text=こんにちは", "text=" + result1.getText());

		UserMessage result2 = resultMessages.get(1);
		assertEquals("id=2", "id=" + result2.getId());
		assertEquals("UserId=2", "UserId=" + result2.getUserId());
		assertEquals("text=よろしくお願いします", "text=" + result2.getText());

		UserMessage result3 = resultMessages.get(2);
		assertEquals("id=3", "id=" + result3.getId());
		assertEquals("UserId=3", "UserId=" + result3.getUserId());
		assertEquals("text=さようなら", "text=" + result3.getText());
	}
}
