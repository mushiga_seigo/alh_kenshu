package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Task;
import com.example.demo.service.TaskNewService;

@Controller
public class TaskNewController {
	public static final String TASK_BLANK = "タスクを入力してください";
	public static final String LIMIT_DATE_BLANK = "期限を設定してください";
	public static final String OVER_CONTENT ="140文字以内で入力してください";
	public static final String INVALID_DATE ="無効な日付です";

	@Autowired
	TaskNewService taskNewService;

	// タスク追加画面初期表示
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Task task = new Task();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", task);
		return mav;
	}

	// タスク追加機能
	@PostMapping("/add")
	public ModelAndView addTask(@ModelAttribute("formModel") Task task, @ModelAttribute("date") String limit_date) {
		List<String> errorMessages = new ArrayList<>();
		Timestamp taskLimit = null;
		//現在時刻取得
		Date nowDate = new Date();
		Timestamp timestamp = new Timestamp(nowDate.getTime());

		//contentチェック
		if ((!StringUtils.hasLength(task.getContent())) & (!StringUtils.hasText(task.getContent()))) {
            errorMessages.add(TASK_BLANK);
        } else if (140 < task.getContent().length()) {
            errorMessages.add(OVER_CONTENT);
        }

		//limit_dateチェック
		if ((!StringUtils.hasLength(limit_date)) & (!StringUtils.hasText(limit_date))) {
			errorMessages.add(LIMIT_DATE_BLANK);
		} else {
			limit_date += " 00:00:00";
			taskLimit = Timestamp.valueOf(limit_date);
			//limit_dateが現在時刻より前の場合エラー表示
			if (taskLimit.before(timestamp)) {
				errorMessages.add(INVALID_DATE);
			}
		}

        if (errorMessages.size() != 0) {
        	ModelAndView mav = new ModelAndView("redirect:/");
        	mav.setViewName("top");
        	mav.addObject("errorMessages", errorMessages);
        	return mav;
        }

		task.setStatus(1);
		// taskをテーブルに格納
		taskNewService.saveTask(task, taskLimit);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
}
