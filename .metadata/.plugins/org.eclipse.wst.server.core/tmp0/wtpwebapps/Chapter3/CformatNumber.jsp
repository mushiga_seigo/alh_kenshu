<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Date"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>fmt:formatNumberのサンプル</title>
</head>
<body>
<h3>fmt:formatNumberのサンプル</h3>
<%
BigDecimal num = new BigDecimal("123456789.112233");
request.setAttribute("number", num);
%>
<fmt:formatNumber value="${number}" pattern="#,##0.##" /> <%-- カンマ区切り、小数点以下2桁まで表示 --%>
</body>
</html>