import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.xml.FlatDtdDataSet;

public class DtdSample {
	public static void main(String[] args) throws Exception {
		// データベースに接続する。
		Class.forName("com.mysql.jdbc.Driver");
		Connection jdbcConnection =
				DriverManager.getConnection("jdbc:mysql://localhost/simple_twitter_database","root","root");
		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
		// Dtdファイルを作成する
		FlatDtdDataSet.write(connection.createDataSet(),
				new FileOutputStream("test.dtd"));
	}
}