/**
 *
 */
package jp.co.JUnit04_FizzBuzz;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author mushiga.seigo
 *
 */
public class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void number9Test() {
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}

	@Test
	public void number20Test() {
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}

	@Test
	public void number45Test() {
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}

	@Test
	public void number44Test() {
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}

	@Test
	public void number46Test() {
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}
}
