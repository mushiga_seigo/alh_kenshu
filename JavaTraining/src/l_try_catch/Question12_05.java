package l_try_catch;

/**
 * 【例外処理】
 * 「Nullの変数は参照している時」に発生する「NullPointerException」が発生しないように
 * if文を使用し、判定をしてみましょう。
 */

public class Question12_05 {

	public static void main(String[] args) {
		String n = null;
		int nLength;

		if(n == null) {
			System.out.println("Nullの変数を参照しているので、returnで処理を終了します。");
			return;
		}
	}

}