package l_try_catch;

/**
 * 【例外処理】
 * 「配列の要素数を超えるインデックスにアクセスした時」に発生する
 * 「ArrayIndexOutOfBoundsException」を発生させてキャッチし、
 * 例外発生時のメッセージをコンソールに表示してみましょう。
 */

public class Question12_03 {

	public static void main(String[] args) {
		int[] values = {1, 2, 3, 4, 5};
		int index = 10;

		try {
			System.out.println(values[index]);
		} catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("配列の要素数を超えるインデックスにアクセスしています。");
			System.out.println(e);
		}
	}

}