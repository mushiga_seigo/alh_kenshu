package l_try_catch;

/**
 * 【例外処理】
 * 「配列の要素数を超えるインデックスにアクセスした時」に発生する
 * 「ArrayIndexOutOfBoundsException」が発生しないように
 * if文を使用し、判定をしてみましょう。
 */

public class Question12_06 {

	public static void main(String[] args) {
		int[] values = {1, 2, 3, 4, 5};
		int num = 10;

		if (num >= values.length) {
			System.out.println("配列の要素を超えるインデックスにアクセスしているので、returnで処理を終了します。");
			return;
		}

		try {
			System.out.println(values[num]);
		} catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("配列の要素数を超えるインデックスにはアクセスできません。");
			System.out.println(e);
		}
	}

}